module Handlers
  ( renderAnimation
  , handleKey
  , renderEntity
  ) where

import SDL

import Types
