module Resources(renderer, happyMerchant, window, grass) where

import System.IO.Unsafe(unsafePerformIO)

import SDL
import qualified SDL.Image as Image

{-# noinline renderer #-}
renderer = unsafePerformIO $ createRenderer window (-1) defaultRenderer
{-# noinline happyMerchant #-}
happyMerchant = unsafePerformIO $ traverse (Image.loadTexture renderer)
  [ "oryx/uf_split/uf_heroes/merchant_b_1.png"
  , "oryx/uf_split/uf_heroes/merchant_b_2.png"
  , "oryx/uf_split/uf_heroes/merchant_b_3.png"
  , "oryx/uf_split/uf_heroes/merchant_b_4.png"
  ]
{-# noinline window #-}
window = unsafePerformIO $ createWindow "My SDL Application" defaultWindow
{-# noinline grass #-}
grass = unsafePerformIO $ traverse (Image.loadTexture renderer)
  [ "oryx/uf_split/uf_terrain/ground_grass.png"
  , "oryx/uf_split/uf_terrain/ground_grass_1.png"
  , "oryx/uf_split/uf_terrain/ground_grass_2.png"
  , "oryx/uf_split/uf_terrain/ground_grass_4.png"
  ]
