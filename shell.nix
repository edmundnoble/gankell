with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "gankell";

  buildInputs = [
    pkgs.zlib pkgs.zlib.dev pkgs.zlib.out
    pkgs.gmp pkgs.gmp.dev pkgs.gmp.out 
    pkgs.libffi pkgs.libffi.out 
    pkgs.SDL2 pkgs.SDL2.out pkgs.SDL2.dev
    pkgs.SDL2_image pkgs.SDL2_image.out
    pkgs.SDL2_mixer pkgs.SDL2_mixer.out 
    pkgs.SDL2_ttf pkgs.SDL2_ttf.out 
    pkgs.pkgconfig
  ];
  LD_LIBRARY_PATH="${zlib}/lib:${gmp}/lib:${libffi}/lib:${SDL2}/lib:${SDL2_image}/lib:${SDL2_mixer}/lib:${SDL2_ttf}/lib";
}

