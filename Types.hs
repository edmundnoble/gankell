{-# language TemplateHaskell #-}

module Types
  ( Keyboard(..)
  , KeyboardF(..)
  , Animation(..)
  , AnimationF(..)
  , PlayerHistory(..)
  , MonsterEntity(..)
  , animF
  , state
  , MonsterState(..)
  , health
  , monsterPos
  , GameState(..)
  , entities
  , keyHandlers
  ) where

import Control.Lens
import Foreign.C.Types

import SDL

data Keyboard = Keyboard
  { _keycode :: !Keycode
  , _lag :: !Double
  , _update :: !(MonsterState -> MonsterState)
  }
newtype KeyboardF = KeyboardF
  { runKeyboardF :: Double -> (KeyboardF, MonsterState -> MonsterState)
  }

newtype Animation = Animation [(Double, Texture)]
newtype AnimationF = AnimationF
  { runAnimationF :: Double -> Point V2 CInt -> IO AnimationF
  }

data MonsterState = MonsterState
  { _health :: Int
  -- , type :: MonsterType
  , _monsterPos :: Point V2 CInt
  }

makeLenses ''MonsterState

data MonsterEntity = MonsterEntity
  { _animF :: !AnimationF
  , _state :: !MonsterState
  }

makeLenses ''MonsterEntity

data GameState = GameState
  { _entities :: ![MonsterEntity],
    _keyHandlers :: ![(Keycode, KeyboardF)]
  -- , projs :: [Projectile]
  }

makeLenses ''GameState

data PlayerHistory = PlayerHistory
  { kills :: Int
  , deaths :: Int
  }