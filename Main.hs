{-# language TemplateHaskell #-}

module Main where

import Control.Lens
import Control.Monad (unless)
import Control.Monad.IO.Class
import Control.Monad.Reader
import Control.Monad.Trans.Cont
import Control.Concurrent(threadDelay)
import Data.Hashable
import Data.StateVar
import Data.Vector(Vector)
import qualified Data.Vector as V
import Foreign.C.Types
import System.IO.Unsafe(unsafePerformIO)

import Linear
import SDL
import qualified SDL.Image as Image
import qualified SDL.Time as Time
import qualified SDL.Video.Renderer as Rend

import Resources
import Types

-- makeLenses ''GameState

main :: IO ()
main = do
  initializeAll
  startScreen

start :: IO GameState
start = do
  merchantAnimF <- renderAnimation cb merchants

  rightHandler' <- handleKey 0.1 (_monsterPos +~ 1)

  return $ GameState [MonsterEntity merchantAnimF (MonsterState 10 (P (V2 10 10)))] rightHandler
  where
  merchants = Animation $ zip [0.5,0.3,0.7,0.4] happyMerchant
  cb = (\loc tex -> Rend.copy renderer tex Nothing (Just loc))

startScreen :: IO ()
startScreen = do
  clear renderer
  rendererDrawColor renderer $= V4 0 0 0 255
  Rend.copy renderer (head happyMerchant) Nothing Nothing
  present renderer
  threadDelay 5000000
  startState <- start
  loop startState

loop :: GameState -> IO ()
loop gameState = do
  events <- pollEvents
  let qPressed = any eventIsQPress events

  newEntities <- render $ entities gameState

  -- 5 milliseconds
  threadDelay 5000

  let moveRight = triggerOnKey KeycodeD $ _entities.singular _head %~ (rightHandler gameState)

  unless qPressed $
    (loop . (_entities .~ newEntities) . moveRight) gameState

  where
  eventIsQPress event =
    case eventPayload event of
      KeyboardEvent keyboardEvent ->
        keyboardEventKeyMotion keyboardEvent == Pressed &&
        elem (keysymKeycode (keyboardEventKeysym keyboardEvent)) [KeycodeQ]
      _ -> False

runKeys :: [(Keycode, KeyboardF)] -> [Event] -> MonsterState -> IO (MonsterState, [KeyboardF])
runKeys keyHandlers events state = do
  now <- Time.time
  (mod, ups) <- flip traverse keyHandlers \(kc, KeyboardF fun) ->
    undefined
  undefined

renderEntity :: (Point V2 CInt -> Point V2 CInt) -> MonsterEntity -> IO MonsterEntity
renderEntity translate (MonsterEntity (Fix animF) state) = do
  t <- Time.time
  newAnimF <- runReaderT animF (t, translate $ monsterPos state)
  return $ MonsterEntity newAnimF state

triggerOnKey :: Keycode -> KeyboardF -> [Event] -> GameState -> IO GameState
triggerOnKey keycode f = go
  where
  go events = if any predicate events then f else id
  predicate event =
    case eventPayload event of
      KeyboardEvent keyboardEvent ->
        keyboardEventKeyMotion keyboardEvent == Pressed &&
        elem (keysymKeycode (keyboardEventKeysym keyboardEvent)) [keycode]
      _ -> False

renderAnimation
  :: (Rectangle CInt -> Texture -> IO ())
  -> Animation
  -> IO AnimationF
renderAnimation rend (Animation allAnims) = do
  start <- Time.time
  animsWithInfo <- traverse getSize allAnims
  return $ Fix $ go start animsWithInfo
  where
  getSize (d,tex) = (,d,tex) <$> queryTextureSize tex
  queryTextureSize tex = do
    info <- queryTexture tex
    return $ V2 (textureWidth info) (textureHeight info)
  go t ((sz,n,tex):ss) = do
    (t', pos) <- ask
    liftIO $ rend (Rectangle pos sz) tex
    pure $ Fix
      if t' - t > n then
        go t' (ss ++ [(sz,n,tex)])
      else
        go t ((sz,n,tex):ss)

handleKey :: Double -> (MonsterState -> MonsterState) -> IO KeyboardF
handleKey lag mod = do
  start <- Time.time
  return $ go start
  where
  go t = KeyboardF $ \t' ->
    if t' - t >= lag then
      (go t', mod)
    else
      (go t, id)

render :: [MonsterEntity] -> IO [MonsterEntity]
render es = do
  clear renderer
  sz <- get $ windowSize window

  let playerVPos = monsterPos (head es^._state)
  let playerRPos = P $ (`div` 2) <$> (sz - tileSize)

  let translate (P x) = P $ unP playerRPos + (x - unP playerVPos) * tileSize

  let tiles      = (div <$> sz <*> tileSize) + 1

  let topLeftCorner     = P $ unP playerVPos - ((`div` 2) <$> tiles)
  let bottomRightCorner = P $ unP playerVPos + ((`div` 2) <$> tiles)

  renderGrid translate topLeftCorner bottomRightCorner

  newEs <- traverse (renderEntity translate) es
  present renderer
  return newEs
  where
  renderGrid translate tl br = do
    forM [tl^._x .. br^._x] $ \x ->
      forM [tl^._y .. br ^._y] $ \y -> do
        let loc = translate (P $ V2 x y)
        let tex = grid V.! (fromIntegral y) V.! (fromIntegral x)
        let rect = Rectangle loc tileSize
        Rend.copy renderer tex Nothing (Just rect)
    -- [V2 x y | x <- tiles^._x, y <- tiles^._y]
    -- (-) <$> ((`div`2) <$> tiles) <*> loc

grid =
  V.generate (gridSize^._y) \y ->
  V.generate (gridSize^._x) \x ->
    grass !! (hash (x * 4, y * 1003) `mod` 4)

gridSize = V2 512 512
tileSize = V2 48 48

